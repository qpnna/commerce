import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ArticleCardComponent } from './article-card/article-card.component';
import { RegisterformComponent } from './registerform/registerform.component';
import { HeaderNavComponent } from './header-nav/header-nav.component';
import { FooterComponent } from './footer/footer.component';
import { ProductGridComponent } from './product-grid/product-grid.component';
import { HttpClientModule } from '@angular/common/http';
import { ProductService } from './service/ProductService';
import { ProductInfoComponent } from './product-info/product-info.component';
import { LoginComponent } from './login/login.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {UserService} from "./service/UserService";
import {AuthenticationService} from "./security/AuthenticationService";
import {RegistrationService} from "./security/RegistrationService"

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ArticleCardComponent,
    RegisterformComponent,
    HeaderNavComponent,
    LoginComponent,
    FooterComponent,
    ProductGridComponent,
    ProductInfoComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    ProductService,
    UserService,
    AuthenticationService,
    RegistrationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
