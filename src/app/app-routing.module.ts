import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {RegisterformComponent} from './registerform/registerform.component';
import {ProductGridComponent} from './product-grid/product-grid.component';
import {ProductInfoComponent} from './product-info/product-info.component';
import {LoginComponent} from "./login/login.component";

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterformComponent },
  { path: 'products', component: ProductGridComponent },
  { path: 'products/:id', component: ProductInfoComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports:[RouterModule]
})
export class AppRoutingModule { }
