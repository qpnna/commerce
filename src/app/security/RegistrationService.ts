import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {UserService} from "../service/UserService";
import {User} from "../model/User";

@Injectable()
export class RegistrationService {
  public currentUser: User;

  constructor(
    private userService: UserService
  ) {
  }

  register(nom: string, prenom: string, email: string, mdp: string, adresse: string, ville: string, cp: string) {
    return this.userService
      .register(nom, prenom, email, mdp, adresse, ville, cp)
      .pipe(map(user => {
        localStorage.setItem('currentUser', JSON.stringify(user));
        return user;
      }));
      console.log(this.userService)
  }
}
