import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {UserService} from "../service/UserService";
import {User} from "../model/User";

@Injectable()
export class AuthenticationService {
  public currentUser: User;

  constructor(
    private userService: UserService
  ) {
  }

  login(username: string, password: string) {
    return this.userService
      .login(username, password)
      .pipe(map(user => {
        localStorage.setItem('currentUser', JSON.stringify(user));
        return user;
      }));
  }

  logout() {
    localStorage.removeItem('currentUser');
  }
}
