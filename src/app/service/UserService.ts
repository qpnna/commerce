import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { User } from '../Model/User';
@Injectable()
export class UserService {
  private static BACKEND = 'http://localhost:3000';

  constructor(private httpClient: HttpClient) {
  }

  public findAll(): Observable<any> {
    return this.httpClient.get(UserService.BACKEND + '/utilisateurs');
  }

  login(username: string, password: string) {
    let params = new HttpParams()
      .append("username", username)
      .append("password", password)
      console.log(params)
    return this.httpClient.post(UserService.BACKEND + '/login', params);
  }

  register(nom: string, prenom: string, email: string, mdp: string, adresse: string, ville: string, cp: string) {
    let params = new HttpParams()
    .append("nom", nom)
    .append("prenom", prenom)
    .append("email", email)
    .append("mdp", mdp)
    .append("adresse", adresse)
    .append("ville", ville)
    .append('cp', cp)
    return this.httpClient.post(UserService.BACKEND + '/utilisateurs', params);
  }
}
