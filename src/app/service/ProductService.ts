import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../model/Product';

@Injectable()
export class ProductService {
    constructor(private httpClient: HttpClient){}

    public findAll(): Observable<any>{
        return this.httpClient.get('http://localhost:3000/produits');
    }
}
