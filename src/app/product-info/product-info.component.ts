import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from '../model/Product';
import { ProductService } from '../service/ProductService';


@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.css']
})
export class ProductInfoComponent implements OnInit {

  product: Product;
  loading = true;

  constructor(
    private route: ActivatedRoute,
    private productService: ProductService
  ) { }

  ngOnInit(): void {

    this.route.paramMap.subscribe(params => {
      console.log( <number><unknown>params.get('id'))
      this.productService.findAll().subscribe((produits: Array<any>) => {
        for (let product of produits) {
          if (<number>product.id == <number><unknown>params.get('id')) {
            console.log('found')
            this.product = new Product(product.id, product.titre, product.description, product.image, product.prix)
            this.loading = false
          }
        }
      })
    })
  }

}
