import { Component, OnInit } from '@angular/core';
import { Product } from '../model/Product';
import { ProductService } from '../service/ProductService';
import { config } from 'rxjs';

@Component({
  selector: 'app-product-grid',
  templateUrl: './product-grid.component.html',
  styleUrls: ['./product-grid.component.css']
})
export class ProductGridComponent implements OnInit {

  products: Product[] = [];

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
      this.productService.findAll().subscribe((produits: Array<any>) => {
        for (let product of produits){
        this.products.push(new Product(product.id, product.titre, product.description, product.image, product.price))
      }

      });
  }

}
