export class User {
  public nom: string;
  public prenom: string;
  public email: string;
  public mdp: number;
  public adresse: string;
  public ville: string;
  public cp: number;

  constructor(nom: string, prenom: string, email: string, mdp: number, adresse: string, ville: string, cp: number) {
    this.nom = nom;
    this.prenom = prenom;
    this.email = email;
    this.mdp = mdp;
    this.adresse = adresse;
    this.ville = ville;
    this.cp = cp;
  }
}
