import { templateSourceUrl, Quote } from '@angular/compiler';
import { ɵBROWSER_SANITIZATION_PROVIDERS } from '@angular/platform-browser';
import { last } from 'rxjs/operators';
import { PipeCollector } from '@angular/compiler/src/template_parser/binding_parser';
import { TokenType } from '@angular/compiler/src/ml_parser/lexer';

export class Product {
    public title: string;
    public description: string;
    public imageUrl: string;
    public price: string;
    public id: number;
  
    constructor(id: number, title: string, description: string, imageUrl: string, price: string) {
        this.id = id
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
        this.price = price;
     }
}