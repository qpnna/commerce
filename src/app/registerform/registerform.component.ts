import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticationService} from "../security/AuthenticationService";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { UserService } from '../service/UserService';
import {RegistrationService} from "../security/RegistrationService";


@Component({
  selector: 'app-registerform',
  templateUrl: './registerform.component.html',
  styleUrls: ['./registerform.component.css']
})
export class RegisterformComponent implements OnInit {
  registerForm : FormGroup

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private registrationService: RegistrationService
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      email: ['', Validators.required],
      mdp: ['', Validators.required],
      adresse: ['', Validators.required],
      ville: ['', Validators.required],
      cp: ['', Validators.required]
    });
  }

  onSubmit(registerForm) {
    console.log(registerForm)
    this.registrationService.register(
      registerForm.nom, 
      registerForm.prenom, 
      registerForm.email, 
      registerForm.mdp, 
      registerForm.adresse, 
      registerForm.ville, 
      registerForm.cp)
      .subscribe(
        data => {
          this.router.navigate(["/"]);
        }
      );
  }
}
